## 🚶🏻 Introduction :

Sonobuoy est un outil conçu pour tester et vérifier la conformité et la santé des clusters Kubernetes. En gros, il effectue deux grandes tâches :

- Tests de Conformité : Sonobuoy exécute un ensemble de tests définis par la Cloud Native Computing Foundation (CNCF) pour s'assurer que votre cluster Kubernetes est conforme aux standards établis. Cela signifie qu'il vérifie si votre cluster fonctionne correctement et est compatible avec les technologies et outils basés sur les normes Kubernetes.

- Diagnostic et Santé : Sonobuoy permet également de réaliser des diagnostics approfondis de votre cluster. Il collecte des informations et des données d'état sur votre cluster, ce qui aide à identifier les problèmes potentiels, comme les configurations incorrectes, les défaillances de noeuds, ou d'autres problèmes de performance et de sécurité.

En résumé, Sonobuoy est comme un outil de santé pour vos clusters Kubernetes, s'assurant qu'ils sont en bonne forme, conformes aux standards de l'industrie, et fonctionnent de manière optimale.

### 🤷🏻‍♂️ Liens utiles :

- https://sonobuoy.io/
- https://sonobuoy.io/docs/main/

## 📚 Documentation d'Installation et d'Utilisation de Sonobuoy

### ⬇️ Téléchargement de Sonobuoy

Pour commencer, téléchargez la dernière version de Sonobuoy depuis les releases GitHub :

```bash
wget https://github.com/vmware-tanzu/sonobuoy/releases/download/v0.57.1/sonobuoy_0.57.1_linux_amd64.tar.gz
```

### 📦 Installation

Une fois téléchargé, extrayez le contenu de l'archive :

```bash
tar -xzf sonobuoy_0.57.1_linux_amd64.tar.gz
```

Cela créera un exécutable `sonobuoy` dans le répertoire courant.

### 🔑 Configuration des permissions

Avant de pouvoir exécuter Sonobuoy, vous devez vous assurer que le fichier est exécutable :

```bash
chmod +x sonobuoy
```

### 🚀 Lancement des Tests

Pour démarrer les tests avec Sonobuoy, utilisez la commande suivante :

```bash
./sonobuoy run
```

Cette commande démarre les tests de conformité et d'autres diagnostics sur votre cluster Kubernetes.

### 📊 Vérification du Statut des Tests

Pour vérifier l'état d'avancement des tests, utilisez :

```bash
./sonobuoy status
```

Cette commande vous fournira des informations sur l'état actuel des tests Sonobuoy en cours.

### 📁 Récupération des Résultats

Une fois les tests terminés, récupérez les résultats avec :

```bash
./sonobuoy retrieve ./results
```

Les résultats seront stockés dans le répertoire `./results` de votre système local.

### 🧹 Nettoyage

Après avoir analysé les résultats, vous pouvez nettoyer les ressources créées par Sonobuoy dans votre cluster avec :

```bash
./sonobuoy delete
```

Cette commande supprimera tous les objets Kubernetes créés par Sonobuoy.

### 📌 Conclusion

En suivant ces étapes, tu pourras intégrer Sonobuoy dans ton cluster Kubernetes, exécuter des tests de conformité et de diagnostic, et examiner les résultats pour assurer la santé et la conformité de ton cluster.